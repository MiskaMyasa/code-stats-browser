$('document').ready(function() {
    var inputToken = $('#token');
    chrome.storage.local.get("token", (items) => {
        if(items.token) {
            inputToken.attr('type', 'password');
            inputToken.val(items.token);
        }
    });

    $('button').click(function() {
        if(inputToken.val().length > 0) {
            chrome.storage.local.set({ "token": inputToken.val() }, function(result){
                inputToken.css('box-shadow','0px 0px 5px green').delay(1000).attr('type', 'password');;
            });
        } else {
            inputToken.css('box-shadow','0px 0px 5px red');
            inputToken.attr('type', 'text');
        }
    });
});